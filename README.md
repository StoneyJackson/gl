# gl

Dockerized GitLab CLI. CLI provided by python-gitlab.

## Run

```bash
docker run -it --rm \
    -v "$HOME/.python-gitlab.cfg:/.python-gitlab.cfg:ro" \
    registry.gitlab.com/stoneyjackson/gl:latest \
    -c /.python-gitlab.cfg <args>...
```

---

# Developer docs

## Local Build

```bash
docker build -t gl:dev .
```

## Local Run

```bash
