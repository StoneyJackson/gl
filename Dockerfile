FROM python:3-alpine
LABEL author="Stoney Jackson"

USER root
RUN pip install python-gitlab

RUN addgroup -S -g 1000 cetacean
RUN adduser -S -u 1000 -G cetacean -s /bin/sh mobydick
WORKDIR /home/mobydick
RUN chown mobydick:cetacean /home/mobydick
USER mobydick

ENTRYPOINT [ "gitlab" ]
